USE tempwatch_esm_home;

CREATE TABLE temp (
   timestamp    INT UNSIGNED NOT NULL,
   sensor_id    VARCHAR(2)   NOT NULL,
   temp         FLOAT        NOT NULL
);
CREATE INDEX temp_uniq ON temp(timestamp, sensor_id, temp);

CREATE TABLE batt (
   timestamp    INT UNSIGNED NOT NULL,
   sensor_id    VARCHAR(2)   NOT NULL,
   batt         FLOAT        NOT NULL
);
CREATE INDEX batt_uniq ON batt(timestamp, sensor_id, batt);


CREATE TABLE location (
    as_of       INT UNSIGNED NOT NULL,
    active      BOOLEAN      NOT NULL,
    sensor_id   VARCHAR(2)   NOT NULL,
    location    VARCHAR(255) NOT NULL,

    PRIMARY KEY(sensor_id)
);

CREATE TABLE threshold (
    sensor_id   VARCHAR(2)  NOT NULL,
    min         FLOAT       NOT NULL,
    max         FLOAT       NOT NULL,
    max_defrost FLOAT       NOT NULL
);

CREATE TABLE history (
    timestamp   INT UNSIGNED NOT NULL,
    sensor_id   VARCHAR(2)   NOT NULL,
    table_name  VARCHAR(255) NOT NULL,
    old_value   VARCHAR(255) NOT NULL,
    new_value   VARCHAR(255) NOT NULL,
    author      VARCHAR(255) NOT NULL,
    row_name    VARCHAR(255) NOT NULL
);

CREATE TABLE notify (
    sensor_id VARCHAR(2)   NOT NULL,
    email     VARCHAR(255) NOT NULL
);

CREATE TABLE sensor_state (
    timestamp   INT UNSIGNED NOT NULL,
    sensor_id   VARCHAR(2)   NOT NULL,
    state       VARCHAR(255) NOT NULL,

    PRIMARY KEY(sensor_id)
);

CREATE TABLE responders (
    rank        INT UNSIGNED NOT NULL,
    name        VARCHAR(255) NOT NULL,
    phone       VARCHAR(20)  NOT NULL,
    last_called INT UNSIGNED NOT NULL
);

CREATE TABLE alerts (
    timestamp   INT UNSIGNED NOT NULL,
    status      VARCHAR(20)  NOT NULL,
    description VARCHAR(255) DEFAULT NULL,
    sensor_id   VARCHAR(2)   NOT NULL,
    responder   VARCHAR(255) DEFAULT NULL,
    next_action VARCHAR(255) DEFAULT NULL,

    PRIMARY KEY (sensor_id)
);

CREATE TABLE alert_log (
    timestamp   INT UNSIGNED NOT NULL,
    responder   VARCHAR(255) NOT NULL,
    action      VARCHAR(255) NOT NULL
);
