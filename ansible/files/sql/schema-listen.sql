USE messages;

DROP TABLE IF EXISTS received;
CREATE TABLE received (
    id           INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    timestamp    INT UNSIGNED NOT NULL,
    sensor_id    VARCHAR(2)   NOT NULL,
    message      VARCHAR(10)  NOT NULL,
    synced       BOOLEAN
);

DROP TABLE IF EXISTS sent;
CREATE TABLE sent (
    id           INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    timestamp    INT UNSIGNED NOT NULL,
    sensor_id    VARCHAR(2)   NOT NULL,
    message      VARCHAR(10)  NOT NULL
);

DROP TABLE IF EXISTS pending;
CREATE TABLE pending (
    id           INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
    sensor_id    VARCHAR(2)   NOT NULL,
    message      VARCHAR(10)  NOT NULL
);
