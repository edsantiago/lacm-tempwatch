/*
** state is one of HIGH, LOW, OK, down, or inactive
*/
CREATE TABLE sensor_state (
    timestamp   INT UNSIGNED NOT NULL,
    sensor_id   VARCHAR(10)  NOT NULL,
    state       VARCHAR(255) NOT NULL,

    PRIMARY KEY(sensor_id)
);

/*
** The most important starting point, from the end user's perspective.
** This is the principal sorting point on the web display, and is what
** should be most prominent if a sensor goes out of bounds.
**
** FIXME: I don't have a good sense for how to describe it, though.
** Should there be a timestamp (for when it gets added/removed/renamed)?
** Or is the history table good enough?
*/
CREATE TABLE cooler_units (
    cooler_id      INT AUTO_INCREMENT PRIMARY KEY, /* users will never see */
    cooler_name    VARCHAR(255) NOT NULL,
    active         TINYINT
);

/* 0 is always treated as unassigned/unallocated */
INSERT INTO cooler_units (cooler_id, cooler_name, active)
                  VALUES (0,"[unassigned]",0);

/*
** How we mostly see the data - each cooler may have one or more sensors,
** and a given sensor can be only in one cooler at a given time, but it
** is expected that sensors will be enabled/disabled, move across coolers,
** even perhaps be renamed (ugh, let's not think about that now).
*/
CREATE TABLE sensor_location (
    as_of          INT UNSIGNED NOT NULL,
    sensor_id      VARCHAR(10)  NOT NULL,     /* Users _do_ need to know */
    cooler_id      INT UNSIGNED,              /* may be NULL if inactive */
    location_name  VARCHAR(255) NOT NULL,     /* brief, e.g "top left" */
    location_full  VARCHAR(255) NOT NULL      /* full desc. of where */
);

/*
** Simple time series of a sensor's readings over time.
*/
CREATE TABLE temp (
    timestamp    INT UNSIGNED NOT NULL,
    sensor_id    VARCHAR(10)  NOT NULL,
    temp         FLOAT        NOT NULL
);
CREATE INDEX temp_uniq ON temp(timestamp, sensor_id, temp);

/* Ditto, but battery voltages. */
CREATE TABLE batt (
    timestamp    INT UNSIGNED NOT NULL,
    sensor_id    VARCHAR(10)  NOT NULL,
    batt         FLOAT        NOT NULL
);
CREATE INDEX batt_uniq ON batt(timestamp, sensor_id, batt);

/*
** Used by xfer-readings to dump messages it can't grok. Some of these
** will be out-of-sequence messages like sensor=A msg=FTEMP... (should
** be sensor=AF msg=TEMP...). Others, well, I guess we'll find out.
*/
CREATE TABLE unhandled_messages (
    timestamp    INT UNSIGNED NOT NULL,
    sensor_id    VARCHAR(10) NOT NULL,
    message      VARCHAR(16)
);

/*
** I thought of per-cooler ranges instead of per-sensor, but different
** locations within the same cooler may have different ranges.
*/
CREATE TABLE temp_thresholds (
    as_of        INT UNSIGNED NOT NULL,
    sensor_id    VARCHAR(10)  NOT NULL,
    min          float        NOT NULL,
    max          float        NOT NULL,
    max_defrost  float
);

/*
** Sensors in a freezer will have lower batt readings than in fridges
*/
CREATE TABLE batt_thresholds (
    as_of        INT UNSIGNED NOT NULL,
    sensor_id    VARCHAR(10)  NOT NULL,
    low          float        NOT NULL,
    warn         float        NOT NULL
);

/*
** Each cooler has (may have) a defrost cycle where temperature may spike.
** A duration of 0 means no cycle - this is appropriate for, e.g., a
** home refrigerator.
*/
CREATE TABLE defrost_cycles (
    as_of        INT     UNSIGNED NOT NULL,
    cooler_id    INT     UNSIGNED NOT NULL,
    frequency    TINYINT UNSIGNED NOT NULL,     /* in hours, probably 6 */
    duration     TINYINT UNSIGNED NOT NULL      /* minutes */
);

/*
** FIXME is this really the right way to go? It's powerful but possibly
** overkill. Maybe a simple timestamp + string would be better?
*/
CREATE TABLE change_history (
    timestamp   INT UNSIGNED NOT NULL,
    table_name  VARCHAR(255) NOT NULL,
    row_name    VARCHAR(255) NOT NULL,
    old_value   VARCHAR(255),
    new_value   VARCHAR(255),
    author      VARCHAR(255)            /* FIXME: index into users table */
);

/*
** While we decide whether or not to use a complex table, here's a simple one.
*/
CREATE TABLE changelog (
    timestamp   INT UNSIGNED NOT NULL,
    log         VARCHAR(255) NOT NULL
);

CREATE TABLE schema_version (
    timestamp   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    version     VARCHAR(255) PRIMARY KEY,
    description VARCHAR(255)
);

INSERT INTO schema_version (version, description)
                    VALUES ('001', 'Initial schema');

/******************************************************************************/
/* FIXME: much more yet to come, e.g. web auth users, email addresses
** for responders, alert history, alert state, twilio call history, ...
*/
