This repo contains a suite of tools used for monitoring temperatures
in refrigeration or freezer cases. Although there are sophisticated
cooler cases on the market, they tend to be expensive; this suite
lets you monitor your units for less than US$200 and a few hours'
time.

This setup was developed for the [Los Alamos Co+op Market][LACM], a
small grocery store in Los Alamos, New Mexico. It has been in use
since March 2016.

Overview
========

The system consists of a [Raspberry Pi][rpi] computer with a
radio card, one or more battery-operated wireless temperature
sensors, a remote (offsite) Linux server, and the software in
this repo. The diagram below offers a simplified concept of
a sample installation:

  ![sample installation](images/lacm-tempwatch.png)

Here we have three **cooler units**, each with one or two
**temperature sensors** (I recommend two: it's good to have backups).
All sensors transmit readings to the **Raspberry&nbsp;Pi**, which then
forwards them on to the **Linux server** where the alert software and
web server run. The web server displays 24-hour or one-week history
plots, letting you see the patterns in your coolers. The web server
also lets you manage defrost cycles and notification settings.

If any sensor goes out of predefined bounds for too long, the Linux
server sends email to a preconfigured address. Optionally, if you
set up an account with the [Twilio](https://www.twilio.com/voice) telecom
provider, the server will start making phone calls to a preconfigured
rotation of phone numbers, e.g. try the store line first, then
the manager, and so on. Calls can be acknowledged by touch-tone
keys, so the system will give the calls a rest.

Requirements
============

See [Hardware](docs/Hardware.md) for the hardware you'll need in-store.
You'll also need a remote Linux server such as provided by
[Linode](https://linode.com/), [AWS](https://aws.amazon.com/), or
any cloud provider. And, of course, some Linux expertise.

_Note:_ you could conceivably run the web server and alerts
directly on the Raspberry Pi, but I don't recommend that: if
you lose power in the store, or the Pi gets unplugged, or you
lose your network connection, you will have no way of finding
out. With a cloud server, a heartbeat monitor alerts you.

 [rpi]:  https://www.raspberrypi.org/help/what-is-a-raspberry-pi/
 [LACM]: http://www.losalamos.coop/
