# -*- perl -*-

use strict;
use warnings;

use Test::More;

(our $ME = $0) =~ s|^.*/||;

# Override time() so we can have consistent test results
BEGIN {
    *CORE::GLOBAL::time = sub() { return 1560951212; };
}

use LACM::TempWatch::Utils qw(friendly_date);

# All tests were written in Ed's home time zone
$ENV{TZ} = 'MST7MDT';

our $Tests = <<'END_TESTS';
0           December 31 1969    # constant time
-1          07:33:31            # 1 second ago
-43200      19:33:32            # half day ago
-43201      Tuesday 19:33       # half day plus one second ago
-86400      Tuesday 07:33       # one day ago
-518400     Thursday 07:33      # six days ago
-518401     June 13             # six days and one second ago
-12960000   January 20          # five months ago
-12960001   January 20 2019     # five months and one second ago
END_TESTS

for my $line (split "\n", $Tests) {
    my $comment;
    if ($line =~ s/\s+#\s+(.*)//) {
        $comment = $1;
    }

    my ($t, $expect) = split ' ', $line, 2;

    my $desc = "friendly_date($t)";
    $desc .= " [$comment]" if $comment;
    $desc .= " = $expect";

    if ($t =~ s/^-//) {
        $t = time - $t;
    }


    is friendly_date($t), $expect, $desc;
}

done_testing();
