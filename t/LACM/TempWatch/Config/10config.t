# -*- perl -*-

use strict;
use warnings;

use File::Temp          qw(tempfile);

use Test::More;

(our $ME = $0) =~ s|^.*/||;

use LACM::TempWatch::Config   qw($Config);

###############################################################################
# BEGIN setup
#
# set up handlers for warn/die, logging their messages
#

my @stderr;
my $rc;

sub reset_config {
    no warnings 'once';
    $LACM::TempWatch::Config::Config = bless {}, 'LACM::TempWatch::Config';

    @stderr = ();
    $rc = undef;
}

$SIG{__DIE__} = sub {
    push @stderr, @_;
    $rc = 1;
};

$SIG{__WARN__} = sub {
    push @stderr, @_;
};

our ($fh, $sample_config) = tempfile("LACM-TempWatch-Config-test.XXXXXXX",
                                 suffix => '.ini');
print { $fh } <<'END_CONFIG';
[a]
b = 1
c = 2
END_CONFIG
close $fh;

# END   setup
###############################################################################
# BEGIN tests

reset_config();

# No config found in search path
my @nonesuch = ('/a/b/c/d', '/d/c/b/a');
@LACM::TempWatch::Config::Search_Path = @nonesuch;
eval { LACM::TempWatch::Config::read(); };
is $rc, 1, 'no config file in search path: throws error';
is_deeply \@stderr, [ "$ME: No config file available: @nonesuch\n"],
    'Error thrown from missing config file';

# Config file found
reset_config();
push @LACM::TempWatch::Config::Search_Path, $sample_config;
eval { LACM::TempWatch::Config::read(); };
is        $rc,      undef, 'config file found: no error';
is_deeply \@stderr, [],    'config file found: no warning messages';

is eval { $Config->required('a', 'b') }, '1', '[a]->b';
is eval { $Config->required('a', 'c') }, '2', '[a]->c';

# 'optional' should give the same results
is eval { $Config->optional('a', 'b') }, '1', '[a]->b';
is eval { $Config->optional('a', 'c') }, '2', '[a]->c';

# 'optional' should be OK with nonexistent section or field
is eval { $Config->optional('a', 'x') }, undef, '[a]->x';
is eval { $Config->optional('x', 'a') }, undef, '[x]->a';

# ...but 'required' should fail differently
reset_config();
eval { $Config->required('a', 'x') };
is $rc, 1, '[a]->x (required): rc';
$stderr[0] =~ s/ at \S+ line .*$//;
is_deeply \@stderr,
    ["$ME: required key 'x' missing from section [a] in $sample_config\n"],
    '[a]->x (required): diagnostic';

reset_config();
eval { $Config->required('x', 'a') };
is $rc, 1, '[x]->a (required): rc';
$stderr[0] =~ s/ at \S+ line .*$//;
is_deeply \@stderr,
    ["$ME: required section [x] missing from $sample_config\n"],
    '[x]->a (required): diagnostic';


# done
unlink $sample_config;
done_testing();
