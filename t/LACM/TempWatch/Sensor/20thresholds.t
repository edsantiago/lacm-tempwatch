# -*- perl -*-

use strict;
use warnings;

use File::Temp          qw(tempdir);

use Test::More;
use Test::Differences;

(our $ME = $0) =~ s|^.*/||;

use LACM::TempWatch::Sensor;

###############################################################################
# BEGIN test values

our $Test_Values = <<'END_TEST_VALUES';
as_of:  10    20    30    40    50    60
max:    40    39    40    50    55    50
min:    10    11    10    20    20    15
END_TEST_VALUES

# Parse
my %Test_Values;
for my $line (split "\n", $Test_Values) {
    $line =~ s/^(\S+):\s+//
        or die;
    my $key = $1;
    my @values = split ' ', $line;

    $Test_Values{$key} = \@values;
}

# END   test values
###############################################################################
# BEGIN setup

# Set up a temp dir with config and db
my $tempdir = tempdir(
    "lacm-tempwatch-sensor.XXXXXXX",
    TMPDIR => 1,
    CLEANUP => !$ENV{DEBUG},
);

# Write a config file
my $cfg = "$tempdir/tempwatch.ini";
my $db  = "$tempdir/foo.sqlite";
open my $fh, '>', $cfg
    or die "$ME: Cannot create $cfg: $!\n";
print { $fh } <<"EOF";
[temp_db]
provider = SQLite
dbname   = $db
EOF
close $fh
    or die "$ME: Error writing $cfg: $!\n";

$ENV{LACM_TEMPWATCH_CONFIG} = $cfg;

########################################
# Initialize the db

my $sqlini = "$tempdir/init.sql";
open $fh, '>', $sqlini
    or die "$ME: Cannot write $sqlini: $!\n";
print { $fh } <<"END_SQL";
CREATE TABLE temp_thresholds (
    as_of        INT UNSIGNED NOT NULL,
    sensor_id    VARCHAR(10)  NOT NULL,
    min          float        NOT NULL,
    max          float        NOT NULL,
    max_defrost  float
);
END_SQL

for my $i (0 .. $#{$Test_Values{as_of}}) {
    my @keys = sort keys %Test_Values;
    printf { $fh } "INSERT INTO temp_thresholds (sensor_id,%s)",join(',',@keys);
    printf { $fh } " VALUES ('AA', %s);\n",
        join(",", map { $Test_Values{$_}[$i] } @keys);
}
close $fh
    or die;

system("sqlite3 $db < $sqlini") == 0
    or die;

# END   setup
###############################################################################
# BEGIN tests

my $s = LACM::TempWatch::Sensor->new('AA');

my @thresholds;

# Simple test with two exact ranges
@thresholds = $s->thresholds(10,20);
eq_or_diff \@thresholds, [ [ 10,10,40], [20,11,39] ], 'simple range';

# Same thing, but within a subrange of the above
@thresholds = $s->thresholds(11,19);
eq_or_diff \@thresholds, [ [ 11,10,40], [19,10,40] ], 'subrange';

# Crossing a change boundary
@thresholds = $s->thresholds(11,29);
eq_or_diff \@thresholds, [ [11,10,40],
                           [20,11,39],
                           [29,11,39],
                       ], 'subrange with a jump';

# undef
@thresholds = $s->thresholds(1,11);
eq_or_diff \@thresholds, [ [ 1,undef,undef],
                           [10,10,40],
                           [11,10,40]], 'undef at the beginning';

# near the end of readings
@thresholds = $s->thresholds(55,99);
eq_or_diff \@thresholds, [ [55,20,55],
                           [60,15,50],
                           [99,15,50]], 'just before the end, one jump';

# And, entire interval is beyond the end
@thresholds = $s->thresholds(99,999);
eq_or_diff \@thresholds, [ [99,15,50],[999,15,50]], 'all past the end';

done_testing;
