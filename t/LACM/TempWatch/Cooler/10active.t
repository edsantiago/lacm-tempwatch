# -*- perl -*-

use strict;
use warnings;

use File::Temp          qw(tempdir);

use Test::More;
use Test::Differences;

(our $ME = $0) =~ s|^.*/||;

use LACM::TempWatch::Cooler;

# FIXME oh dear fsm this has to be refactored

###############################################################################
# BEGIN test values

our %db_init;

$db_init{cooler_units} = <<'END_COOLER_UNITS';
cooler_id | cooler_name           | active
0         |  [unassigned]         |  0
1         |  Freezer              |  1
2         |  Cheese Case          |  1
3         |  Cheese Island (gone) |  0
4         |  Meat Case            |  1
5         |  Grab and Go          |  1
6         |  Walk-in              |  1
END_COOLER_UNITS

$db_init{sensor_state} = <<'END_SENSOR_STATE';
sensor_id  | state
AA         |  OK
AB         |  OK
AC         |  HIGH
AD         |  LOW
AE         |  inactive
AF         |  OK
AG         |  OK
AH         |  OK
AI         |  OK
AJ         |  OK
END_SENSOR_STATE

$db_init{sensor_location} = <<'END_SENSOR_LOCATION';
sensor_id | cooler_id | location_name
AA        |  1        |  Upper Left
AB        |  1        |  Upper Right
AC        |  2        |  -
AD        |  4        |  Lower Left
AE        |  0        |  SHOULD NEVER SEE THIS ONE
AF        |  4        |  Lower Right
AG        |  5        |  -
AH        |  5        |  -
AI        |  5        |  -
AJ        |  6        |  -
END_SENSOR_LOCATION

# END   test values
###############################################################################
# BEGIN setup

# Set up a temp dir with config and db
my $tempdir = tempdir(
    "lacm-tempwatch-sensor.XXXXXXX",
    TMPDIR => 1,
    CLEANUP => !$ENV{DEBUG},
);

# Write a config file
my $cfg = "$tempdir/tempwatch.ini";
my $db  = "$tempdir/foo.sqlite";
open my $fh, '>', $cfg
    or die "$ME: Cannot create $cfg: $!\n";
print { $fh } <<"EOF";
[temp_db]
provider = SQLite
dbname   = $db
EOF
close $fh
    or die "$ME: Error writing $cfg: $!\n";

$ENV{LACM_TEMPWATCH_CONFIG} = $cfg;

########################################
# Initialize the db

my $sqlini = "$tempdir/init.sql";
open $fh, '>', $sqlini
    or die "$ME: Cannot write $sqlini: $!\n";
print { $fh } <<"END_SQL";
CREATE TABLE cooler_units (
    cooler_id      INT UNSIGNED PRIMARY KEY,    /* Users should never see */
    cooler_name    VARCHAR(255) NOT NULL,
    active         TINYINT
);

CREATE TABLE sensor_state (
    timestamp   INT UNSIGNED,
    sensor_id   VARCHAR(10)  NOT NULL,
    state       VARCHAR(255) NOT NULL,

    PRIMARY KEY(sensor_id)
);

CREATE TABLE sensor_location (
    as_of          INT UNSIGNED,
    sensor_id      VARCHAR(10)  NOT NULL,     /* Users _do_ need to know */
    cooler_id      INT UNSIGNED,              /* may be NULL if inactive */
    location_name  VARCHAR(255),     /* brief, e.g "top left" */
    location_full  VARCHAR(255)      /* full desc. of where */
);

CREATE TABLE temp (
    as_of          INT UNSIGNED,
    sensor_id      VARCHAR(10),
    temp           FLOAT
);
END_SQL

for my $table (sort keys %db_init) {
    # Parse
    my @fields;
    for my $line (split "\n", $db_init{$table}) {
        my @x = map { s/^\s+//; s/\s+$//; ($_) } split('\|', $line);
        $x[-1] = '' if $x[-1] eq '-';   # FIXME: special handling of blanks
        if (! @fields) {
            @fields = @x;
        }
        else {
            printf { $fh } "INSERT INTO $table (%s) VALUES (\"%s\");\n",
                join(',', @fields),
                join('","', @x);
        }
    }
}
close $fh
    or die;

system("sqlite3 $db < $sqlini") == 0
    or die;

# END   setup
###############################################################################
# BEGIN tests

my @coolers = LACM::TempWatch::Cooler::active_coolers();

is scalar(@coolers), 5, "number of active coolers";

# Cooler names. Remember, they're sorted now
my @expect = ('Cheese Case', 'Freezer', 'Grab and Go', 'Meat Case', 'Walk-in');
for my $i (0..4) {
    is $coolers[$i]->name, $expect[$i], "Cooler[$i]->name = $expect[$i]";
}

# Sensor IDs
@expect = ( ['AC'], ['AA','AB'], ['AG','AH','AI'], ['AD','AF'], ['AJ']);
for my $i (0..4) {
    my @sensors = $coolers[$i]->sensors;
    my @sensor_ids = map { $_->id } @sensors;

    my $cooler_name = $coolers[$i]->name;
    eq_or_diff \@sensor_ids, $expect[$i], "$cooler_name: sensor IDs";
}

# Sensor names and states
@expect = (
    [ 'Cheese Case=HIGH'                                                     ],
    [ 'Freezer (Upper Left)=OK',    'Freezer (Upper Right)=OK'               ],
    [ 'Grab and Go=OK',             'Grab and Go=OK',       'Grab and Go=OK' ],
    [ 'Meat Case (Lower Left)=LOW', 'Meat Case (Lower Right)=OK'             ],
    [ 'Walk-in=OK'                                                           ],
);
for my $i (0..4) {
    my @sensors = $coolers[$i]->sensors;

    my $cooler_name = $coolers[$i]->name;
    my @sensor_names = map { $_->location . '=' . $_->{state} } @sensors;

    eq_or_diff \@sensor_names, $expect[$i], "$cooler_name: sensor name/state";
}

# Remove and add sensors on a cooler
my $ah = ($coolers[2]->sensors())[1];
$coolers[2]->remove_sensor($ah);
$coolers[4]->add_sensor($ah);

# AH should be gone from where it was, and added to the last element
@expect = ( ['AC'], ['AA','AB'], ['AG','AI'], ['AD','AF'], ['AJ','AH']);
for my $i (0..4) {
    my @sensors = $coolers[$i]->sensors;
    my @sensor_ids = map { $_->id } @sensors;

    my $cooler_name = $coolers[$i]->name;
    eq_or_diff \@sensor_ids, $expect[$i], "$cooler_name: sensor IDs";
}


done_testing();
