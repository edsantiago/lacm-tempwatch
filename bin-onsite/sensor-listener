#!/usr/bin/perl
#
# sensor-listener - listen for reports from sensors, stash them locally
#
package LACM::SensorListener;

use strict;
use warnings;
use utf8;
use v5.10;

(our $ME = $0) =~ s|.*/||;
(our $VERSION = '$Revision: 0.0 $ ') =~ tr/[0-9].//cd;

# For debugging, show data structures using DumpTree($var)
#use Data::TreeDumper; $Data::TreeDumper::Displayaddress = 0;

###############################################################################
# BEGIN user-customizable section

# For connecting to transceiver
our $SerialPort;

# For making sure there isn't more than one process running
our $Lockfile = "/tmp/$ME.serial.lock";

# Connection to database
our $Message_DB;

# FIXME
our @Pending_Messages;

# END   user-customizable section
###############################################################################

use FindBin qw($Bin);
use lib "$Bin/../lib";

use LACM::TempWatch::Config     qw($Config);
use LACM::TempWatch::DB;
use Device::SerialPort;

###############################################################################
# BEGIN boilerplate args checking, usage messages

sub usage {
    print  <<"END_USAGE";
Usage: $ME [OPTIONS]

$ME is part of the LACM::TempWatch package. This is
the part that listens for reports from temperature sensors. We run
on the on-site Raspberry Pi, connect to the transceiver via serial
port, then wait for reports from sensors. As they come in, we write
them into a local mysql database.

OPTIONS:

  --config PATH  path to .ini file with our settings

  -v, --verbose  show verbose progress indicators
  -n, --dry-run  make no actual changes

  --help         display this message
  --man          display program man page
  --version      display program name and version
END_USAGE

    exit;
}

sub man {
    # Read the POD contents.  If it hasn't been filled in yet, abort.
    my $pod = do { local $/; <DATA>; };
    if ($pod =~ /=head1 \s+ NAME \s+ FIXME/xm) {
        warn "$ME: No man page available.  Please try $ME --help\n";
        exit;
    }

    # Use Pod::Man to convert our __DATA__ section to *roff
    eval { require Pod::Man }
        or die "$ME: Cannot generate man page; Pod::Man unavailable: $@\n";
    my $parser = Pod::Man->new(name => $ME, release => $VERSION, section => 1);

    # If called without output redirection, man-ify.
    my $out_fh;
    if (-t *STDOUT) {
        my $pager = $ENV{MANPAGER} || $ENV{PAGER} || 'less';
        open $out_fh, "| nroff -man | $pager";
    }
    else {
        open $out_fh, '>&STDOUT';
    }

    # Read the POD contents, and have Pod::Man read from fake filehandle.
    # This requires 5.8.0.
    open my $pod_handle, '<', \$pod;
    $parser->parse_from_filehandle($pod_handle, $out_fh);
    exit;
}


# Command-line options.  Note that this operates directly on @ARGV !
our $debug   = 0;
our $force   = 0;
our $verbose = 0;
our $NOT     = '';              # print "blahing the blah$NOT\n" if $debug
sub handle_opts {
    use Getopt::Long;
    GetOptions(
        'config=s'   => sub { LACM::TempWatch::Config::read($_[1]) },

        'debug!'     => \$debug,
        'dry-run|n!' => sub { $NOT = ' [NOT]' },
        'force'      => \$force,
        'verbose|v'  => \$verbose,

        help         => \&usage,
        man          => \&man,
        version      => sub { print "$ME version $VERSION\n"; exit 0 },
    ) or die "Try `$ME --help' for help\n";
}

# END   boilerplate args checking, usage messages
###############################################################################

############################## CODE BEGINS HERE ###############################

# The term is "modulino".
__PACKAGE__->main()                                     unless caller();

# Main code.
sub main {
    # Note that we operate directly on @ARGV, not on function parameters.
    # This is deliberate: it's because Getopt::Long only operates on @ARGV
    # and there's no clean way to make it use @_.
    handle_opts();                      # will set package globals

    die "$ME: Too many arguments; try $ME --help\n"                 if @ARGV;

    $SerialPort = connect_to_serial_port();
    $Message_DB = LACM::TempWatch::DB->new('message_db');

    main_loop();
}


###############
#  main_loop  #
###############
sub main_loop {
    while (1) {
        my ($sensor_id, $message);

        # send_pending_commands() calls read_message(), so it can get
        # acknowledgment of sent commands. But of course there may be
        # messages received from other sensors. Those are queued up
        # in @Pending_Messages, which we process first.
        if (@Pending_Messages) {
            ($sensor_id, $message) = @{ shift @Pending_Messages };
        }
        else {
            # No queued messages; block waiting for one.
            ($sensor_id, $message) = read_message();
        }

        # Special case: a sensor has just booted up. There is a small
        # window in which we can send it commands, such as changing
        # the reporting interval or even renaming the ID.
        if ($message =~ /^START/) {
            send_pending_commands($sensor_id);
        }
    }
}


###############################################################################
# BEGIN serial port stuff

############################
#  connect_to_serial_port  #
############################
#
# Initializes the connection to the serial port on the radio transceiver.
#
sub connect_to_serial_port {
    my $dev = $Config->required('sensor_listener', 'device');
    my $connection = Device::SerialPort->new($dev, 1, $Lockfile)
        or die "$ME: Cannot open serial port $dev: $!\n";

    if (my $baud = $Config->optional('sensor_listener', 'baudrate')) {
        $connection->baudrate($baud);
    }
    $connection->read_const_time(500);
    $connection->read_char_time(5);

    return $connection;
}


##################
#  read_message  #
##################
#
# Blocks waiting for a radio message. If we get one, return sensor ID and
# message content.
#
# From what I can tell, the protocol is that all messages are announced
# with the letter 'a'. There then follow eleven (11) more characters:
# a two-character sensor ID, then 9 characters of message data. Message
# data is usually a temperature or battery voltage, or AWAKE/SLEEPING.
# If message is shorter than 9 characters, it is right-padded with
# minus signs:
#
#   AHTEMP006.8         - TEMP indicates degrees Celsius (old sensors)
#   BBAWAKE----
#   BBTMPA3.90-         - TMPA indicated Fahrenheit (new sensors)
#   BBSLEEPING-
#   AHBATT2.59-
#
# Here we perform serial reads, keeping a FIFO with incoming characters.
# When the FIFO grows to 12 characters, we treat it as a message and
# return it to our caller as (2-character sensor_id, 9-character message).
#
# Because our transceiver may initialize in the middle of a message,
# and simply because of Murphy, we discard (with a warning) any
# character sequence that does not begin with 'a'.
#
sub read_message {
    state $fifo = '';

    # Does not actually loop forever: we return as soon as we have a message.
    while (1) {
        # This can happen on restart, but hopefully never again.
        # It means we're not properly synced. All we can do is
        # discard until an 'a'.
        if ($fifo =~ s/^([^a]+)//) {
            warn "$ME: WARNING: discarding out-of-sync sequence '$1'\n";
        }

        # Each message is exactly 12 characters: an 'a' sync byte, and
        # an 11-character message.
        if (length($fifo) >= 12) {
            if (substr($fifo, 0, 1, '') eq 'a') {
                # Pop off the 11-character message
                my $sensor_id = substr($fifo, 0, 2, '');  # eg AA
                my $message   = substr($fifo, 0, 9, '');  # eg TMPA-20.5

                $message =~ s/[\s-]+$//;    # trim trailing dashes/spaces
                printf " [ msg: sensor=$sensor_id %-11s ]\n", "'$message'"
                    if $debug;

                log_message($sensor_id, $message);
                return ($sensor_id, $message);
            }
            else {
                warn "$ME: INTERNAL ERROR!! Message fifo '$fifo' does not begin with 'a'";
                # Continue. What else can we do?
            }
        }

        # FIFO does not have the necessary 12 characters. Read some more.
        my ($count, $string) = $SerialPort->read(99);

        if ($count > 0) {
            print "[ read: <$count> '$string' ]\n" if $debug;

            # FIXME: can this happen?
            if (length($string) != $count) {
                my $l = length($string);
                warn "$ME: WARNING: SerialPort->read() returned count=$count but string='$string' ($l characters)";
            }

            $fifo .= $string;
        }
    }
}


##################
#  send_message  #  Send a serial-bus message directed at a given sensor
##################
sub send_message {
    my $sensor_id = shift;
    my $message   = shift;

    # FIXME: this seems to be necessary
    sleep 1;

    print "[ ->: $sensor_id -> $message ]\n"                    if $debug;
    $SerialPort->write("a${sensor_id}${message}");

    # Wait up to 10 seconds for acknowledgment.
    my $wait_until = time + 10;
    while (time < $wait_until) {
        my ($ack_sensor_id, $ack_message) = read_message();
        if ($ack_sensor_id eq $sensor_id) {
            if ($ack_message eq $message) {
                return 1;                       # Yay
            }
            else {
                warn "$ME: Response, not ack, from $sensor_id: $ack_message\n";
            }
        }

        # Not our desired ack. Preserve it anyway for main_loop() to process.
        push @Pending_Messages, [ $ack_sensor_id, $ack_message ];
    }

    warn "$ME: No ack from $sensor_id on $message\n";
    return;
}

# END   serial port stuff
###############################################################################
# BEGIN database stuff

#################
#  log_message  #  Log receipt of a sensor message
#################
sub log_message {
    my $sensor_id = shift;
    my $message   = shift;

    $Message_DB->do(<<'END_SQL', time, $sensor_id, $message, 0);
INSERT INTO received (timestamp, sensor_id, message, synced)
             VALUES  (        ?,         ?,       ?,      ?)
END_SQL
}


###########################
#  send_pending_commands  #  Checks for and sends updates to sensor on START
###########################
#
# When a sensor is powered on (by inserting battery), it sends a START
# message. At this point there's a small window during which it will
# stay awake, and we can send it configuration commands.
#
# Config commands are written by the sensor-update script to the DB,
# where they sit happily until we see a START.
#
# This code almost never runs, because (1) config changes are rare,
# and (2) so are STARTs.
#
sub send_pending_commands {
    my $sensor_id = shift;

    # Check for pending commands. If none (the usual case), bail.
    my @pending_commands = $Message_DB->do(<<'END_SQL', $sensor_id);
SELECT * FROM pending WHERE sensor_id=?  ORDER BY id
END_SQL
    return if ! @pending_commands;

    # Got something. Quick, send an AWAKE message during the start window!
    send_message($sensor_id, 'AWAKE')
        or return;

    # Send each requested command
    for my $cmd (@pending_commands) {
        # FIXME: if we don't get an ack, we won't send CYCLE! Try harder?
        send_message($sensor_id, $cmd->{message})
            or return;

        # FIXME: delete from pending_commands
        # FIXME: keep an activity log somewhere!
        $Message_DB->do('DELETE FROM pending WHERE id=?', $cmd->{id});

        # Special case: if we're renaming a sensor, change our ID now too;
        # otherwise subsequent pending commands won't get to it and neither
        # will the CYCLE at the end.
        if ($cmd->{message} =~ /^CHDEVID(..)$/) {
            $sensor_id = $1;
        }
    }

    # FIXME: try multiple times?
    send_message($sensor_id, 'CYCLE');
}


# END   database stuff
###############################################################################

1;

__DATA__

###############################################################################
#
# Documentation
#

=head1	NAME

FIXME - description of what this script does

=head1	SYNOPSIS

FIXME [B<--foo>]  [B<--bar>]  [B<--verbose>] ARG1 [ARG2...] FIXME

FIXME  B<--help>  |  B<--version> | B<--man>

=head1	DESCRIPTION

B<FIXME> grobbles the frobniz on alternate Tuesdays, except where
prohibited by law.

=head1	OPTIONS

=over 4

=item B<--foo>

FIXME

=item B<--verbose>

Show progress messages.

=item B<--help>

Emit usage hints.

=item B<--version>

Display program version.

=item B<--man>

Display this man page.

=back


=head1	DIAGNOSTICS

FIXME

=head1	ENVIRONMENT

FIXME

=head1	FILES

FIXME

=head1	RESTRICTIONS

FIXME

=head1	SEE ALSO

FIXME

e.g. L<Foo::Bar|Foo::Bar>

=head1	AUTHOR

Your Name <ed@edsantiago.com>

Please report bugs or suggestions to <ed@edsantiago.com>

=cut
