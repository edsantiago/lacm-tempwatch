<!--- format with: markdown_py README.md > README.html -->

Overview
========

This document describes a system used for monitoring temperatures
at the [Los Alamos Co+op Market][lacm]

 [lacm]: http://www.losalamos.coop/

Hardware
========

As of June 2019 this setup consists of a [Raspberry Pi][rpi] computer,
a **radio transceiver** that plugs into the Pi, and about eight
battery-operated wireless **temperature sensors**. The transceiver
and sensors we have were made by a company that is no longer in
business, but a company in Toronto has started manufacturing
equivalent devices. I (Ed) ordered some in December&nbsp;2017 and
can confirm that they work just as well as the ones in our store.
Shipping only took one week.

You will need:

* one **[Raspberri Pi][rpi]**. I recommend the **3B** because it has
  built-in WiFi--on older Pis you need a USB WiFi adapter and we've
  had terrible luck with those. If you don't know much about Pis,
  be sure to get a **kit**: it will include heat sinks (with instructions
  on how to install), a power cable, and a case. All of those are
  important.
* one **[Raspbian SD card][raspbian]**. You probably have an 8/16GB microSD
  card somewhere or can buy one, so it's easiest to download the Raspbian
  image and copy it to your card. (Left as an exercise for the reader).
* one **[transceiver][transceiver]**. I recommend getting a spare. Have I
  mentioned that I like having backups?
* several **[temperature sensors][sensors]**.  I recommend two for each
  cooler unit, and as many spares as you can afford -- we've had to
  replace two sensors because of flaky radio transmission. Getting
  them ahead of time saves you agony if the new company stops making
  sensors or changes their specs.
* several **CR2032 batteries**. Yes, I know the product web page
  claims that the sensors include batteries. They don't. You need
  to buy your own. This unfortunately makes the sensors more
  expensive.
* an offsite **Linux server** hosted in a reliable datacenter. If
  you don't know what this means, or don't have the ability to set
  one up, contact me. I might let you piggyback on one of mine.
  (This is offered in the cooperative spirit, and is contingent
  on my time and financial constraints.)
* a **[Twilio](https://www.twilio.com/)** account, if you want
  phone-call notifications. I recommend this. You will very likely
  be fine with their free plan, that's what we're using at LACM.

 [rpi]:         https://www.raspberrypi.org/products/
 [raspbian]:    https://www.raspbian.org/RaspbianImages
 [transceiver]: https://www.jemrf.com/products/wireless-base-station-for-raspberry-pi
  [sensors]:     https://www.jemrf.com/products/wireless-temperature-sensor

Initial Setup
=============

See [Setup-Pi.md](Setup-Pi.md)
