Setting up the Raspberry Pi
===========================

This document describes initial setup of the RPi. It assumes you have
the RPi connected to a LAN where you can ping it and ssh to it.
Installing Debian on the RPi, and getting it connected to the network,
is left as an exercise for the reader. The rest is:

* Connecting the transceiver
* Finding the right magic settings to configure the lacm-tempwatch software
* Installing the lacm-tempwatch software on the RPi
* Running it

Connecting the Transceiver
==========================

It's been a long time since I last did this. The key elements you may
need to know are:

* Disabling serial console:
```
pi# raspi-config
  -> Interfacing Options (or, if Serial not there, try Advanced Options)
     -> Serial
        -> No

(Reboot if it changed from Yes)

pi# systemctl mask serial-getty@ttyS0.service  (might be ttyAMA0 on old Pis)

pi# echo enable_uart = 1 >> /boot/config.txt   (I'm not 100% sure this is needed)
```

If you are comfortable with minicom, try:
```
pi# minicom -w -b 9600 -D /dev/ttyS0    (or AMA0 on old Pis)
```
Type `+++` and see if you get an OK. If you don't, maybe try 115200
instead of 9600; or try just waiting a while and see if you get any
messages from nearby temperature sensors - they would look like `AATEMP20.0`
or possibly `AATMPA20.0`. Don't bother with any further steps until
the transceiver is working - this is the trickiest part of the operation.

For further reference see also
[this archived reddit thread](https://www.reddit.com/r/IOT/comments/567k13/slice_of_radio_by_wirelessthings_ciseco_setup/),
where someone mercifully brought in many of the original Ciseco instructions.
You can ignore everything from pySerial onward - we will not be using the
Ciseco MessageBridge software.


Magic Settings
==============

Set up mysql on the RPi. Sorry, again, this is left as an exercise
for the reader. Once you have mysql running and configured:

```
pi# pwgen -B -y 20
....list of many randomly-generated passwords, e.g. ey9Ahghei~xoo4shaeN
pi# mysql -u root
mysql> CREATE USER 'message_listener'@'localhost' IDENTIFIED BY 'ey9Ahghei~xoo4shaeN';
mysql> CREATE DATABASE messages;
mysql> GRANT ALL ON messages TO 'message_listener'@'localhost';
```
Copy `ansible/files/sql/schema-listen.sql` to the RPi, and run:
```
pi# mysql -u message_listener -P < schema-listen.sql
Password: <paste in the password here>
```

Now edit `ansible/inventory`, setting hostnames, addresses, usernames,
passwords, and TCP ports as necessary.

Install software on RPi
=======================

On a system on the same network as the RPi, e.g. your laptop, run:
```
controller$ cd ansible
controller$ ansible-playbook -i inventory --verbose deploy-monitor.yml
```
This should take 5 minutes or so. When it's done, your RPi will have
all the necessary software to talk to the transceiver and log
messages from the temperature sensors. It should be running, you
can confirm by running this as the unprivileged user on the RPi:
```
pi$ systemctl --user status sensor-listener.service|cat
● sensor-listener.service - Log temperature packets from sensors
   Loaded: loaded (/home/tempwatch/.config/systemd/user/sensor-listener.service; enabled; vendor preset: enabled)
   Active: active (running) since Tue 2019-06-11 19:37:00 MDT; 23h ago
 Main PID: 4900 (sensor-listener)
   CGroup: /user.slice/user-1003.slice/user@1003.service/sensor-listener.service
           └─4900 /usr/bin/perl /home/tempwatch/bin/sensor-listener
```
If you see anything other than 'active (running)', you'll need to
troubleshoot. First thing to try would be running the listener directly:
```
pi$ ./bin/sensor-listener --debug
```
It should just sit there, spitting out messages every few minutes
(assuming you have sensors nearby). If it errors out, debug and
please let me know so I can fix the code and/or update docs.

At this point you should also be able to ssh from your cloud server
back to the RPi. Test that before you move the RPi to the store
network, and/or before you go home.

FIXME FIXME FIXME: that's all I have for now (2019-06-12). This will
be updated as time allows.
