# -*- perl -*-
#
# LACM::TempWatch::Config - find and read db & other config settings
#
package LACM::TempWatch::Config;

use strict;
use warnings;

use Carp;
use Config::Std;
use File::stat;

###############################################################################
# BEGIN user-configurable section

our @Search_Path = ("$ENV{HOME}/.tempwatch.ini", "/etc/tempwatch.ini");

our $Config_File;

# FIXME: add ->required and ->optional methods

our $Config = bless {}, __PACKAGE__;

# END   user-configurable section
###############################################################################

# Program name of our caller
(our $ME = $0) =~ s|.*/||;

# For non-OO exporting of code, symbols
our @ISA         = qw(Exporter);
our @EXPORT      = qw();
our @EXPORT_OK   = qw($Config);
our %EXPORT_TAGS =   (all => \@EXPORT_OK);

##########
#  read  #  Read config settings from a file
##########
sub read {
    my $self = shift;
    my $path = shift || _find_config();

    # Already initialized?
    if (($Config->{_read_from}||'') eq $path) {
        if ($Config->{_read_mtime} == stat($path)->mtime) {
            return;
        }
    }

    read_config $path => my %c;

    $Config->{config}      = \%c;
    $Config->{_read_from}  = $path;
    $Config->{_read_mtime} = stat($path)->mtime;
}


##################
#  _find_config  #  Find first available config file
##################
sub _find_config {
    # First priority: set by caller (probably from command-line option)
    return $Config_File if $Config_File;

    # Second priority: environment variable
    if (my $path = $ENV{LACM_TEMPWATCH_CONFIG}) {
        return $path;
    }

    # Finally, try the search path
    for my $path (@Search_Path) {
        return $path if -e $path;
    }

    die "$ME: No config file available: @Search_Path\n";
}

##############
#  required  #  Mandatory setting
##############
sub required {
    my $self    = shift;
    my $section = shift || croak "$ME: missing SECTION";
    my $key     = shift || croak "$ME: missing KEY";

    $self->read();
    exists $self->{config}{$section}
        or croak "$ME: required section [$section] missing from $self->{_read_from}";
    exists $self->{config}{$section}{$key}
        or croak "$ME: required key '$key' missing from section [$section] in $self->{_read_from}";

    return $self->{config}{$section}{$key};
}

sub optional {
    my $self    = shift;
    my $section = shift || croak "$ME: missing SECTION";
    my $key     = shift || croak "$ME: missing KEY";

    return $self->{config}{$section}{$key};
}

1;


###############################################################################
#
# Documentation
#

=head1	NAME

FIXME - FIXME

=head1	SYNOPSIS

    use Fixme::FIXME;

    ....

=head1	DESCRIPTION

FIXME fixme fixme fixme

=head1	CONSTRUCTOR

FIXME-only if OO

=over 4

=item B<new>( FIXME-args )

FIXME FIXME describe constructor

=back

=head1	METHODS

FIXME document methods

=over 4

=item	B<method1>

...

=item	B<method2>

...

=back


=head1	EXPORTED FUNCTIONS

=head1	EXPORTED CONSTANTS

=head1	EXPORTABLE FUNCTIONS

=head1	FILES

=head1	SEE ALSO

L<Some::OtherModule>

=head1	AUTHOR

Ed Santiago <ed@edsantiago.com>

=cut
