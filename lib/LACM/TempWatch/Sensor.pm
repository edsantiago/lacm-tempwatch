# -*- perl -*-
#
# LACM::TempWatch::Sensor - code relating to temperature sensors
#
package LACM::TempWatch::Sensor;

use strict;
use warnings;
use utf8;

use Carp;
use LACM::TempWatch::DB;
use LACM::TempWatch::Utils      qw(friendly_date);

###############################################################################
# BEGIN user-configurable section

# persistent database handle
our $DB;

# Useful time constants
our $HOUR =  3600;      # seconds
our $DAY  = 86400;

# END   user-configurable section
###############################################################################

# Program name of our caller
(our $ME = $0) =~ s|.*/||;


#################
#  _initialize  #  Connect to database
#################
sub _initialize {
    $DB //= LACM::TempWatch::DB->new('temp_db');
}


sub all_sensors {
    _initialize();

    my %all_sensor_ids;

    for my $table (qw(temp sensor_state sensor_location)) {
        $all_sensor_ids{ $_->{sensor_id} } = 1 for $DB->do(<<"END_SQL");
SELECT DISTINCT(sensor_id) FROM $table
END_SQL
    }

    return map { LACM::TempWatch::Sensor->new($_) } (sort keys %all_sensor_ids);
}

############
#  active  #  all active sensors
############
sub active {
    _initialize();

    my @active;
    for my $s (all_sensors()) {
        push @active, $s  if $s->cooler_id;
    }

    return sort {
        $a->cooler_name  cmp $b->cooler_name       ||
       $a->location_name cmp $b->location_name     ||
              $a->id     cmp $b->id
          } @active;
}

#########
#  new  #  constructor
#########
sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $arg   = shift || croak "Invoked $class->new() without args";
    croak "Too many args to $class->new()" if @_;

    _initialize();

    my $self = {};
    if ((ref($arg)||'') eq 'HASH') {
        $self->{id} = $arg->{sensor_id};
        # FIXME: look in %$args for other fields?
        $self->{$_} = $arg->{$_} for keys(%$arg);
    }
    elsif ($arg =~ /^([A-Z0-9]{2})$/) {
        $self->{id} = $1;
    }
    else {
        croak "invalid args to ".__PACKAGE__."->new(): '$arg'";
    }

    return bless $self, $class;
}

###############
#  heartbeat  #  Timestamp of the most recent data received
###############
sub heartbeat {
    _initialize();

    my @result = $DB->do(<<'END_SQL');
SELECT timestamp FROM temp ORDER BY timestamp DESC LIMIT 1
END_SQL
    return $result[0]->{timestamp} if @result;
    return 0;
}

################
#  _load_info  #  Load sensor info (static, e.g. location) from database
################
sub _load_info {
    my $self = shift;

    if (! exists $self->{_info_loaded}) {
        my $sth = $DB->prepare(<<'END_SQL');
SELECT * FROM sensor_location l
         JOIN cooler_units c ON (l.cooler_id = c.cooler_id)
         WHERE l.sensor_id=?
         ORDER BY as_of DESC LIMIT 1
END_SQL
        $sth->execute($self->{id});
        # It's OK if we find no info, e.g. new sensors, disabled ones
        if (my $row = $sth->fetchrow_hashref) {
            $self->{$_} = $row->{$_} for keys %$row;
        }

        my @state = $DB->do('SELECT * FROM sensor_state WHERE sensor_id=?',
                            $self->{id});
        if (@state) {
            $self->{state} = $state[0]{state};
        }

        $self->{_info_loaded} = 1;
    }
}

###############################################################################
# BEGIN simple accessors

sub get {
    my $self = shift;
    my $want = shift;

    $self->_load_info();
    return $self->{$want} || '';
}

sub id {
    my $self = shift;
    return $self->{id};
}

sub temperature {
    my $self = shift;
    my $format = shift;                 # in: optional: sprintf format

    my ($temp, $ts) = $self->_latest('temp');
    $self->{temperature}           = $temp;
    $self->{temperature_timestamp} = $ts;

    if ($format) {
        if (defined $self->{temperature}) {
            return sprintf($format, $self->{temperature});
        }
        return 'N/A';
    }
    return $self->{temperature};
}

sub state {
    my $self = shift;

    if (my $temp = $self->temperature) {
        return 'lost'       if $self->{temperature_timestamp} < (time - $DAY);
        return 'lost_maybe' if $self->{temperature_timestamp} < (time - $HOUR);
        return 'late'       if $self->{temperature_timestamp} < (time - 600);
        return 'OK';
    }

    return 'lost';
}

sub battery {
    my $self = shift;
    my $format = shift;                 # in: optional: sprintf format

    my ($batt, $ts) = $self->_latest('batt');
    $self->{battery}           = $batt;
    $self->{battery_timestamp} = $ts;

    if ($format) {
        if (defined $self->{battery}) {
            return sprintf($format, $self->{battery});
        }
        return 'N/A';
    }
    return $self->{battery};
}

sub battery_state {
    my $self = shift;

    if (my $voltage = $self->battery) {
        return 'lost' if $self->{battery_timestamp} < (time - 1 * $DAY);

        # FIXME: don't hardcode temps
        return 'low'  if $voltage < 2.1;
        return 'warn' if $voltage < 2.3;
        return 'ok';
    }

    return 'lost';
}

#############
#  _latest  #  Latest temperature or battery reading
#############
sub _latest {
    my $self  = shift;
    my $table = shift;                  # in: 'temp' or 'batt'

    my $sth = $DB->prepare(<<"END_SQL");
SELECT * FROM $table WHERE sensor_id=? ORDER BY timestamp DESC LIMIT 1
END_SQL
    $sth->execute($self->{id});

    if (my $row = $sth->fetchrow_hashref) {
        return ($row->{$table}, $row->{timestamp});
    }

    return (undef, undef);
}


sub cooler_name {
    my $self = shift;

    $self->_load_info();
    return $self->{cooler_name} || '';
}

sub cooler_id {
    my $self = shift;

    $self->_load_info();
    return $self->{cooler_id} || 0;
}

sub location_name {
    my $self = shift;
    $self->_load_info();
    return $self->{location_name} || '';
}
sub location_full {
    my $self = shift;
    $self->_load_info();
    return $self->{location_full} || '';
}

sub location {
    my $self = shift;

    $self->_load_info();

    my $loc = $self->{cooler_name};
    if (my $desc = $self->{location_name}) {
        $loc .= " ($desc)";
    }
    return $loc;
}

# END   simple accessors
###############################################################################
# BEGIN more complicated stuff

################
#  thresholds  #  min/max table for a given time period
################
sub thresholds {
    my $self = shift;
    my ($t0, $t1) = @_;

    my %thresholds = (
        $t0 => [ undef, undef ],
        $t1 => [ undef, undef ],
    );

    my @t0 = $DB->do(<<'END_SQL', $self->{id}, $t0);
SELECT * FROM temp_thresholds
         WHERE sensor_id=?
         AND   as_of <= ?
      ORDER BY as_of DESC
         LIMIT 1
END_SQL

    my ($min, $max);
    if (@t0) {
        # FIXME: need to get defrost cycles, see if we're in one
        ($min, $max) = ($t0[0]{min}, $t0[0]{max});
        $thresholds{$t0} = [ $min, $max ];
    }

    # Now find upper limit
    my @t1 = $DB->do(<<'END_SQL', $self->{id}, $t0, $t1);
SELECT * FROM temp_thresholds
         WHERE sensor_id=?
         AND   as_of >= ?
         AND   as_of <= ?
END_SQL

    for my $t (@t1) {
        ($min, $max) = ($t->{min}, $t->{max});
        $thresholds{ $t->{as_of} } = [ $min, $max ];
    }

    # Final one
    if (defined $min) {
        $thresholds{ $t1 } = [ $min, $max ];
    }

    return map { [ $_, @{$thresholds{$_}} ] } sort keys %thresholds;
}

#######################
#  current_threshold  #  Finds the most recent min/max/defrost
#######################
sub current_threshold {
    my $self = shift;
    my $which = shift;          # in: min, max, defrost_max

    my @result = $DB->do(<<'END_SQL', $self->id);
SELECT * FROM temp_thresholds
        WHERE sensor_id=?
     ORDER BY as_of DESC
        LIMIT 1
END_SQL

    if (@result) {
        return $result[0]{$which};
    }
    return;
}

sub temperature_history {
    my $self = shift;
    my ($t0, $t1) = @_;

    my @history;

    my @thresholds = $self->thresholds($t0, $t1);
    my $threshold_index = 0;

    my $sth = $DB->prepare(<<'END_SQL');
SELECT * FROM temp
         WHERE sensor_id=?
         AND timestamp >= ? AND timestamp <= ?
         ORDER BY timestamp ASC
END_SQL
    $sth->execute($self->{id}, $t0, $t1);
    while (my $row = $sth->fetchrow_hashref) {
        # FIXME: check for low or high
        my ($ts, $temp) = ($row->{timestamp}, $row->{temp});

        # See if we bump up to a new minmax
        if ($threshold_index < $#thresholds) {
            if ($ts >= $thresholds[$threshold_index+1][0]) {
                $threshold_index++;
            }
        }
        my $state = 'ok';
        if ($temp < ($thresholds[$threshold_index][1] // -999)) {
            $state = 'low';
        }
        elsif ($temp > ($thresholds[$threshold_index][2] // 999)) {
            $state = 'high';
        }

        push @history, [ $row->{timestamp}, $row->{temp}, $state ];
    }

    return @history;
}

###############
#  last_seen  #  Friendly calendar date of when we received the last reading
###############
sub last_seen {
    my $self = shift;

    my @last_seen = $DB->do(<<'END_SQL', $self->{id});
SELECT timestamp FROM temp
                WHERE sensor_id=?
             ORDER BY TIMESTAMP DESC LIMIT 1
END_SQL

    if (@last_seen) {
        return friendly_date($last_seen[0]{timestamp});
    }
    return 'Never';
}

# END   more complicated stuff
###############################################################################
# BEGIN update

sub update_location {
    my $self    = shift;
    my $updates = shift;                # in: hashref of changes to make
    my $log     = shift;                # in: descriptive log message

    # FIXME: should we generate the changelog here instead of our caller?
    my $sql = <<'END_SQL';
INSERT INTO sensor_location
            (as_of, sensor_id, cooler_id, location_name, location_full)
     VALUES (    ?,         ?,         ?,             ?,             ?)
END_SQL

    $DB->do($sql, time, $self->id, $self->{cooler_id},
            $updates->{location_name} || $self->location_name || '',
            $updates->{location_full} || $self->location_full || '');

    if ($log) {
        $sql = "INSERT INTO changelog (timestamp, log) VALUES (?,?)";
        $DB->do($sql, time, "Sensor " . $self->{id} . ": " . $log);
    }
}


sub update_thresholds {
    my $self    = shift;
    my $updates = shift;                # in: hashref of changes to make
    my $log     = shift;                # in: descriptive log message

    my @fields = sort keys %$updates;
    my @values = map { $updates->{$_} } @fields;

    # FIXME: should we generate the changelog here instead of our caller?
    my $sql = <<'END_SQL';
INSERT INTO temp_thresholds (as_of, sensor_id, min, max, max_defrost)
                     VALUES (    ?,         ?,   ?,   ?,           ?)
END_SQL
    $DB->do($sql,time,$self->id,map { $updates->{$_} } qw(min max max_defrost));

    if ($log) {
        $sql = "INSERT INTO changelog (timestamp, log) VALUES (?,?)";
        $DB->do($sql, time, "Sensor " . $self->{id} . ": " . $log);
    }
}


# END   update
###############################################################################

1;


###############################################################################
#
# Documentation
#

=head1	NAME

FIXME - FIXME

=head1	SYNOPSIS

    use Fixme::FIXME;

    ....

=head1	DESCRIPTION

FIXME fixme fixme fixme

=head1	CONSTRUCTOR

FIXME-only if OO

=over 4

=item B<new>( FIXME-args )

FIXME FIXME describe constructor

=back

=head1	METHODS

FIXME document methods

=over 4

=item	B<method1>

...

=item	B<method2>

...

=back


=head1	EXPORTED FUNCTIONS

=head1	EXPORTED CONSTANTS

=head1	EXPORTABLE FUNCTIONS

=head1	FILES

=head1	SEE ALSO

L<Some::OtherModule>

=head1	AUTHOR

Ed Santiago <ed@edsantiago.com>

=cut
