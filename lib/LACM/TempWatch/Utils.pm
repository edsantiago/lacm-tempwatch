# -*- perl -*-
#
# LACM::TempWatch::Utils - helpers with no other home
#
package LACM::TempWatch::Utils;

use strict;
use warnings;

use Carp;
use Time::Piece;

###############################################################################
# BEGIN user-configurable section

use constant DAY  => 86400;              # seconds
use constant DAYS => 86400;              # seconds

# END   user-configurable section
###############################################################################

# Program name of our caller
(our $ME = $0) =~ s|.*/||;

# RCS id, accessible to our caller via "$<this_package>::VERSION"
(our $VERSION = '$Revision: 0.0 $ ') =~ tr/[0-9].//cd;

# For non-OO exporting of code, symbols
our @ISA         = qw(Exporter);
our @EXPORT      = qw();
our @EXPORT_OK   = qw(friendly_date friendly_mtime);
our %EXPORT_TAGS =   (all => \@EXPORT_OK);


###################
#  friendly_date  #  Human-readable date in the past
###################
sub friendly_date {
    my $t = shift;

    my $lt = localtime($t);
    my $delta_t = time - $t;

    return $lt->hms                     if $delta_t <= DAY / 2;
    return $lt->strftime("%A %H:%M")    if $delta_t <= 6 * DAYS;
    return $lt->strftime("%B %e")       if $delta_t <= 5 * 30 * DAYS;
    return $lt->strftime("%B %e %Y");
}

####################
#  friendly_mtime  #  Human-readable delta-t
####################
sub friendly_mtime {
    my $delta_t = shift;

    my @units = ([ 60, 'seconds' ], [ 60, 'minutes' ], [ '24', 'hours' ]);
    for my $unit (@units) {
        if ($delta_t <= 2 * $unit->[0]) {
            return sprintf("%d %s", $delta_t, $unit->[1]);
        }
        $delta_t /= $unit->[0];
    }

    return sprintf("%d days", $delta_t);
}


1;


###############################################################################
#
# Documentation
#

=head1	NAME

FIXME - FIXME

=head1	SYNOPSIS

    use Fixme::FIXME;

    ....

=head1	DESCRIPTION

FIXME fixme fixme fixme

=head1	CONSTRUCTOR

FIXME-only if OO

=over 4

=item B<new>( FIXME-args )

FIXME FIXME describe constructor

=back

=head1	METHODS

FIXME document methods

=over 4

=item	B<method1>

...

=item	B<method2>

...

=back


=head1	EXPORTED FUNCTIONS

=head1	EXPORTED CONSTANTS

=head1	EXPORTABLE FUNCTIONS

=head1	FILES

=head1	SEE ALSO

L<Some::OtherModule>

=head1	AUTHOR

Ed Santiago <ed@edsantiago.com>

=cut
