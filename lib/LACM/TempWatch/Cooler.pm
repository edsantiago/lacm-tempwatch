# -*- perl -*-
#
# LACM::TempWatch::Cooler - code relating to individual cooler units
#
package LACM::TempWatch::Cooler;

use strict;
use warnings;
use utf8;

use Carp;
use LACM::TempWatch::DB;
use LACM::TempWatch::Sensor;

###############################################################################
# BEGIN user-configurable section

# persistent database handle
our $DB;

# END   user-configurable section
###############################################################################

# Program name of our caller
(our $ME = $0) =~ s|.*/||;


#################
#  _initialize  #  Connect to database
#################
sub _initialize {
    $DB //= LACM::TempWatch::DB->new('temp_db');
}


#########
#  all  #  all coolers
#########
sub all_coolers {
    _initialize();

    my @results = $DB->do(<<'END_SQL');
SELECT * FROM cooler_units c
        WHERE cooler_id > 0
     ORDER BY cooler_name ASC
END_SQL

    return map { LACM::TempWatch::Cooler->new( $_ ) } @results;
}

############
#  active  #  all active coolers
############
sub active_coolers {
    _initialize();

    return grep { scalar($_->sensors) >= 1 } all_coolers();
}

#########
#  new  #  constructor
#########
sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $arg   = shift // croak "Invoked $class->new() without args";
    croak "Too many args to $class->new()" if @_;

    _initialize();

    my $self = {};
    if ((ref($arg)||'') eq 'HASH') {
        $self->{id} = $arg->{cooler_id};
        $self->{$_} = $arg->{$_} for keys(%$arg);
    }
    elsif ($arg =~ /^(\d+)$/) {
        $self->{id} = $1;
        # FIXME: do queries to populate name!
    }
    else {
        # FIXME: will there ever be a need to query by name?
        croak "invalid args to ".__PACKAGE__."->new(): '$arg'";
    }

    return bless $self, $class;
}

###############################################################################
# BEGIN simple accessors

sub id   { return $_[0]->{id} }
sub name { return $_[0]->{cooler_name} }

#############
#  sensors  #  Sorted list of sensors in this cooler
#############
sub sensors {
    my $self = shift;

    _initialize();

    if (! exists $self->{_sensors}) {
        $self->{_sensors} = [ sort {
            ($a->location_name||'') cmp ($b->location_name||'')
                         || $a->id  cmp  $b->id
        } grep {
            ($_->cooler_id||0) == $self->{id}
        } LACM::TempWatch::Sensor::all_sensors() ];

    }

    return @{ $self->{_sensors} };
}

# END   simple accessors
###############################################################################
# BEGIN update functions used by web UI

###################
#  remove_sensor  #  Remove a sensor from our list (BUT NOT IN THE DB)
###################
sub remove_sensor {
    my $self = shift;
    my $sensor_to_remove = shift;       # Must be obj. FIXME: allow just sid?

    # FIXME: confirm that we found it?
    my @newlist = grep { $_->id ne $sensor_to_remove->id } $self->sensors();
    $self->{_sensors} = \@newlist;
}

################
#  add_sensor  #  Add a sensor to our list (BUT NOT IN THE DB)
################
sub add_sensor {
    my $self = shift;
    my $sensor_to_add = shift;       # Must be obj. FIXME: allow just sid?

    $sensor_to_add->{cooler_id}   = $self->{id};
    $sensor_to_add->{cooler_name} = $self->{cooler_name};

    my @newlist = ($self->sensors, $sensor_to_add);
    $self->{_sensors} = \@newlist;
}


################
#  add_cooler  #  Create a new cooler
################
#
# Returns numeric ID on success, string error on failure
#
sub add_cooler {
    my $self = shift;
    my $name = shift;

    _initialize();

    $name
        or return "Name cannot be empty";
    $name =~ /^[A-Z]/
        or return "Name must begin with capital letter";
    $name =~ /[a-z]/
        or return "Name cannot be ALL CAPS";

    if ($DB->do("SELECT * FROM cooler_units WHERE cooler_name=?",$name)) {
        return "Cooler already exists";
    }

    # Looks good! Let's give this a try.
    my @results = $DB->do(<<'END_SQL', $name);
INSERT INTO cooler_units (cooler_name, active) VALUES (?, 1)
END_SQL

    return $results[0];
}

# END   update functions used by web UI
###############################################################################
# BEGIN defrost cycles

sub defrost_cycles {
    my $self = shift;
    my ($t0, $t1) = @_;

    # FIXME
    ...
}

# END   defrost cycles
###############################################################################

1;


###############################################################################
#
# Documentation
#

=head1	NAME

FIXME - FIXME

=head1	SYNOPSIS

    use Fixme::FIXME;

    ....

=head1	DESCRIPTION

FIXME fixme fixme fixme

=head1	CONSTRUCTOR

FIXME-only if OO

=over 4

=item B<new>( FIXME-args )

FIXME FIXME describe constructor

=back

=head1	METHODS

FIXME document methods

=over 4

=item	B<method1>

...

=item	B<method2>

...

=back


=head1	EXPORTED FUNCTIONS

=head1	EXPORTED CONSTANTS

=head1	EXPORTABLE FUNCTIONS

=head1	FILES

=head1	SEE ALSO

L<Some::OtherModule>

=head1	AUTHOR

Ed Santiago <ed@edsantiago.com>

=cut
